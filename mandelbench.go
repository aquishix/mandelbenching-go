package main

import "fmt"
import "time"

func main() {
	dwell := 1000000

	//fmt.Print("Testing?")
	/*
	   fmt.Print("O")
	   fmt.Print(" ")
	   fmt.Print("O")
	   fmt.Print(" ")
	*/

	   var escaped bool = false
	   var x int = 0
	   var y int = 0
	   var cim float64 = 0.0
	   var cre float64 = 0.0
	   var nre float64 = 0.0
	   var nim float64 = 0.0
	   var re  float64 = 0.0
	   var im  float64 = 0.0
	   var its int     = 0
	   var total_its int64  = 0
           var ti int64 = 0
           var tf int64 = 0
           var elapsed float64 = 0.0 

        /* 
	escaped := false
	x := 0
	y := 0
	cim := 0.0
	cre := 0.0
	nre := 0.0
	nim := 0.0
	re := 0.0
	im := 0.0
	its := 0
        total_its := 0        
         
        ti := time.Now().UnixNano()/1000000
        tf := ti
        elapsed := 0.0
        */

        ti = time.Now().UnixNano()/1000000
	for y = 19; y >= -20; y-- {
		fmt.Println("")
		cim = float64(y) / 20.0
		for x = -40; x <= 39; x++ {
			cre = float64(x) / 40.0 - 0.5
			re = 0.0
			im = 0.0

			escaped = false
			for its = 0; (!escaped && its < dwell); its++ {
				nre = re*re - im*im
				nim = 2 * re * im
				re = nre + cre
				im = nim + cim

                                total_its++
				if (re*re + im*im) >= 4.0 {
					escaped = true
				}
			}
			if its >= 1000 {
				fmt.Print(" ")
			} else {
				fmt.Print("O")
			}

		}
		//fmt.Println("")
	}
        fmt.Println("")
        fmt.Print("Total iterations calculated: ")
        fmt.Println(total_its)
        tf = time.Now().UnixNano()/1000000
        elapsed = (float64(tf-ti))/1000.0
        fmt.Print("Elapsed time: ")
        fmt.Print(elapsed)  
        fmt.Println(" seconds.")

        fmt.Println(ti)
        fmt.Println(tf)

}
